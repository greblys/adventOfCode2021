def parse():
    f = open("input.txt", "r")
    lines = f.readlines()
    lines = map(lambda x: x.strip(), lines)
    balls = map(int, lines[0].split(","))
    lines = lines[2:]
    for i, line in enumerate(lines):
        lines[i] = map(int, line.split())

    boards = []
    for i in range(0, len(lines), 6):
        boards.append(lines[i:i+5])

    return balls, boards


def is_completed_board(board):
    for row in board:
        if all(map(lambda x: x == 0, row)):
            return True

    for y in range(5):
        is_column_completed = True
        for x in range(5):
            if board[x][y] != 0:
                is_column_completed = False
                break
        if is_column_completed:
            return True

    return False


def mark_board(ball, board):
    for y in range(5):
        for x in range(5):
            if board[y][x] == ball:
                board[y][x] = 0
                return


def get_score(board):
    score = 0
    for row in board:
        score += sum(row)
    return score * balls[i]


balls, boards = parse()
score = 0
is_completed = False
i = 0

while not is_completed and i < len(balls):
    for board in boards:
        mark_board(balls[i], board)
        if is_completed_board(board):
            score = get_score(board)
            is_completed = True
            break
    i += 1

for board in boards:
    for line in board:
        print line
    print

print("Score: {}".format(score))
