def parse():
    f = open("input.txt", "r")
    lines = f.readlines()
    lines = map(lambda x: x.strip(), lines)
    balls = map(int, lines[0].split(","))
    lines = lines[2:]
    for i, line in enumerate(lines):
        lines[i] = map(int, line.split())

    boards = []
    for i in range(0, len(lines), 6):
        boards.append(lines[i:i+5])

    return balls, boards


def is_completed_board(board):
    for row in board:
        if all(map(lambda x: x == "X", row)):
            return True

    for x in range(5):
        is_column_completed = True
        for y in range(5):
            if board[y][x] != "X":
                is_column_completed = False
                break
        if is_column_completed:
            return True

    return False


def mark_board(ball, board):
    for y in range(5):
        for x in range(5):
            if board[y][x] == ball:
                board[y][x] = "X"
                return

def replace_markers(board):
    for y in range(5):
        for x in range(5):
            if board[y][x] == "X":
                board[y][x] = 0

def get_score(board):
    replace_markers(board)
    score = 0
    for row in board:
        score += sum(row)
    return score * balls[i-1]

def print_boards():
    print i, balls[i]
    for board in boards:
        for line in board:
            print line
        print


balls, boards = parse()
is_game_completed = False
i = 0

while not is_game_completed and i < len(balls):
    for board in boards:
        mark_board(balls[i], board)
        if is_completed_board(board) and len(boards) > 1:
            boards.remove(board)
        elif is_completed_board(board) and len(boards) == 1:
            is_game_completed = True

    print_boards()
    i += 1

print("Score: {} {}".format(get_score(boards[0]), i))
