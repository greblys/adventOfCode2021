f = open("input.txt", "r")
depths = map(int, f.readlines())
last = None
count = 0
i = 0
while i + 2 < len(depths):
    window = depths[i] + depths[i+1] + depths[i+2]
    if last is not None and last < window:
        count += 1
    last = window
    i += 1

print("{} measurements larger than previous one".format(count))
