f = open("input.txt", "r")
depths = map(int, f.readlines())
last = depths[0]
count = 0

for depth in depths[1:]:
    if depth > last:
        count += 1
    last = depth

print("{} measurements larger than previous one".format(count))
