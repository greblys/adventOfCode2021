def parse(line):
    (direction, val) = line.split()
    return direction, int(val)


instructions = {
    "forward": lambda v: (x + v, y),
    "down": lambda v: (x, y + v),
    "up": lambda v: (x, y - v)
}

f = open("input.txt", "r")
commands = map(parse, f.readlines())
x = 0
y = 0
for command in commands:
    (x, y) = instructions[command[0]](command[1])

print("Multiplication of depth and horizontal position: {}".format(x * y))
