def parse(line):
    (direction, val) = line.split()
    return direction, int(val)


instructions = {
    "forward": lambda v: (x + aim * v, y + v, aim),
    "down": lambda v: (x, y, aim + v),
    "up": lambda v: (x, y, aim - v)
}

f = open("input.txt", "r")
commands = map(parse, f.readlines())
x = 0
y = 0
aim = 0
for command in commands:
    (x, y, aim) = instructions[command[0]](command[1])

print("Multiplication of depth and horizontal position: {}".format(x * y))
