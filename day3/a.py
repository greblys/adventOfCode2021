def get_bit_count(pos):
    count = 0
    for n in binaries:
        if n & (pow(2, pos)) > 0:
            count += 1
    return count


f = open("input.txt", "r")
binaries = map(lambda x: int(x, 2), f.readlines())
gamma = 0
epsilon = 0
list = range(12)
list.reverse()
for i in list:
    gamma = gamma << 1
    epsilon = epsilon << 1
    if get_bit_count(i) > len(binaries) / 2:
        gamma += 1
    else:
        epsilon += 1

print("Submarine power consumption: {}".format(gamma * epsilon))
