import copy


def get_bit_count(pos, subset):
    count = 0
    for n in subset:
        if n & (pow(2, pos)) > 0:
            count += 1
    return count


f = open("input.txt", "r")
binaries = map(lambda x: int(x, 2), f.readlines())
oxygen = copy.deepcopy(binaries)
co2 = copy.deepcopy(binaries)
rng = range(12)
rng.reverse()
for i in rng:
    if len(oxygen) > 1:
        if get_bit_count(i, oxygen) >= len(oxygen) / 2.0:
            oxygen = filter(lambda x: x & (pow(2, i)) > 0, oxygen)
        else:
            oxygen = filter(lambda x: x & (pow(2, i)) == 0, oxygen)

    if len(co2) > 1:
        if get_bit_count(i, co2) >= len(co2) / 2.0:
            co2 = filter(lambda x: x & (pow(2, i)) == 0, co2)
        else:
            co2 = filter(lambda x: x & (pow(2, i)) > 0, co2)
    print(len(oxygen), len(co2))

print("Life support rating: {}".format(oxygen[0] * co2[0]))
